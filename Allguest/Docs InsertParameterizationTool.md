**Snippet Name:** `update_db_receipts`

**Requirements** 
   
   _Files_
   - **Excel file with name:** `Datos_parametrizacion.xlsx`  
   
   _System requirements_
   - **Node js** `^v16.16` 

**Description**  
The InsertParameterizationTool is designed to migrate, create and insert new parameterization configurations to the Allguest web portal 

With this tool you can 

- Insert subcatalog products massively 
- Delete/add old and unissued desks, Hotels and Groups  
- Change the relationship of any of the 3 components of this schema (Desks, Hotels and Groups)

**Parameters:**
- `Excel file:`Datos_parametrizacion.xlsx

follow this link to download the template for this file, its needed to make it follo

**How it works**

It will begin erasing old fields on the database and also anything related info of this tables 

![image](/uploads/1f94848a60655ff2f87aa43fdf1b00cb/image.png)
