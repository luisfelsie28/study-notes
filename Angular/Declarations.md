Las declarations se utilizan para definir todas las vistas correspondientes a un modulo,

por ejemplo, vamos a tomar una landing page y que todo se esta definiendo dentro de un mismo modulo

en ese caso dentro de las declarations del modulo deberiamos ver algo asi:
```js
@NgModule({

  declarations: [

    Home,
    AboutUs,
    Buy

  ],
```


esto nos indica que nuestro modulo va a tener 3 vistas, que si lo compararmos con programacion orientada a objetos, seria el equivalente a decir que estas son los atributos de la clase 
