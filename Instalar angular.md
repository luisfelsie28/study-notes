Como instalar angular

# Requisitos 
 
	Node Lts
	NPM 

Ver version de [[node]]
`node -v`
**Resultado:**
`v14.17.3`
A parte de node, tambien necesitaremos un manejador de paquetes, en este caso [[npm]] 
Un equivalente podria ser [[pip]] de python

Ver version de [[npm]]
`npm -v`
**Resultado:**
`6.14.13`

# Angular

Con los requisitos ya satisfechos, es necesario instalar la angular CLI 
funciona de manera similiar a la CLI de [[aws]] o cualquier sistema de cloud

en ella encontraremos cmandos que nos seran de utilidad para inicializar un proyecto por ejemplo

Para inicializar cualquier proyecto
en este caso podriamos usar 
```bash
ng new <proyecto>
ng serve
```







